# Dockerfile Android/C++/CMake Development Environment

From [mizux/cpp](https://hub.docker.com/r/mizux/cpp/):
* [ubuntu:latest](https://hub.docker.com/_/ubuntu/)
* [GCC/G++ 4.5.2](http://packages.ubuntu.com/xenial/build-essential)
* [CMake 3.5.1](http://packages.ubuntu.com/xenial/cmake)
* [Doxygen 1.8.11](http://packages.ubuntu.com/xenial/doxygen)
* [CppCheck 1.72](http://packages.ubuntu.com/xenial/cppcheck)
* [Qt5 5.5.1](http://packages.ubuntu.com/xenial/qtbase5-dev)

This layer(s) add:
* curl
* unzip
* OpenJDK8 (jvm & jre)
* Android NDK r12b
* Android SDK 24.4.1 (build-tools-24.0.2,tools,platform-tools,android-19 aka 4.4.2)
* Qt5.5 armv7 (in /usr/local/android_armv7)

with ENV:  
* JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
* ANDROID_HOME=/usr/local/android-sdk-linux
* ANDROID_SDK_HOME=/usr/local/android-sdk-linux
* ANDROID_NDK_HOME=/usr/local/android-ndk
* PATH=${ANDROID_HOME}/tools:${ANDROID_HOME}/platform-tools:${ANDROID_HOME}/build-tools/24.0.2:$PATH
* ANDROID_QT_HOME=/usr/local/android_armv7

## Docker Build
```sh
docker build -t mizux/android .
```

## Run
```sh
docker run -it mizux/android bash
```

