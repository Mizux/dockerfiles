# Dockerfile C++/CMake Development Environment

* [ubuntu:latest](https://hub.docker.com/_/ubuntu/)
* [GCC/G++ 4.5.2](http://packages.ubuntu.com/xenial/build-essential)
* [CMake 3.5.1](http://packages.ubuntu.com/xenial/cmake)
* [Doxygen 1.8.11](http://packages.ubuntu.com/xenial/doxygen)
* [CppCheck 1.72](http://packages.ubuntu.com/xenial/cppcheck)
* [Qt5 5.5.1](http://packages.ubuntu.com/xenial/qtbase5-dev)

## Docker Build
```sh
docker build -t mizux/cpp .
```

## Run
```sh
docker run -it mizux/cpp bash
```

